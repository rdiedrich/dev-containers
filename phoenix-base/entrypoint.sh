#!/bin/sh

set -e

cd /app
mix setup
iex -S mix phx.server
