#!/bin/sh

NAME=webmon-prod

export DATABASE_URL=ecto://postgres:postgres@localhost/$NAME
export SECRET_KEY_BASE=$(mix phx.gen.secret)
export PORT=4001
export POOL_SIZE=2
export PHX_SERVER=true
export PHX_HOST=localhost

podman pod create --name $NAME-pod \
       --replace=true \
       -p $PORT:$PORT

podman run --name $NAME-db --pod $NAME-pod \
       --replace=true \
       -e POSTGRES_PASSWORD=postgres \
       -e POSTGRES_DB=$NAME \
       -d postgres:14

podman run --name $NAME-app --pod $NAME-pod \
       --replace=true \
       -e PHX_HOST -e PORT -e PHX_SERVER \
       -e DATABASE_URL -e POOL_SIZE -e SECRET_KEY_BASE \
       -d localhost/$NAME
