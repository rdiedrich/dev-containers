#!/bin/sh

podman pod create --name phoenix-pod \
       --replace=true \
       -p 4000:4000

podman run --name phoenix-db --pod phoenix-pod \
       --replace=true \
       -e POSTGRES_PASSWORD=postgres \
       -d postgres:14

podman run --name phoenix-app --pod phoenix-pod \
       --replace=true \
       -v $(pwd):/app \
       -it localhost/phoenix-base
